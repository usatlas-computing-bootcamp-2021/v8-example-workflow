#!/bin/bash
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase;source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
cd /home/eresseguie/v8-example-workflow/batch/
asetup AnalysisBase,21.2.186
source ../build/x86_64-centos7-gcc8-opt/setup.sh 
AnalysisPayload $1
