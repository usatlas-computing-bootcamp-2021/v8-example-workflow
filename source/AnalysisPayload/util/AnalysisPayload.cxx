// stdlib functionality
#include <iostream>
// ROOT functionality
#include <TFile.h>
#include <TH1D.h>
// ATLAS EDM functionality
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODJet/JetContainer.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODMuon/MuonContainer.h"

// Add a submodule for JetSelectionHelper
#include "JetSelectionHelper/JetSelectionHelper.h"
#include "ElectronSelectionHelper/ElectronSelectionHelper.h"
#include "MuonSelectionHelper/MuonSelectionHelper.h"

// jet calibration
#include <AsgTools/AnaToolHandle.h>
#include <JetCalibTools/IJetCalibrationTool.h>

int main(int argc, char** argv) {

  // initialize the xAOD EDM
  xAOD::Init();

  // open the input file
  TString inputFilePath = "/work/eresseguie/bootcamp/DAOD_EXOT27.24604725._000001.pool.root.1";
  if(argc >= 2) inputFilePath = argv[1];
  xAOD::TEvent event;
  std::unique_ptr< TFile > iFile ( TFile::Open(inputFilePath, "READ") );
  if(!iFile) return 1;
  event.readFrom( iFile.get() );

  // make histograms for storage
  TH1D *h_njets_raw = new TH1D("h_njets_raw","",20,0,20);
  TH1D *h_njets_kin = new TH1D("h_njets_kin","",20,0,20);

  TH1D *h_mjj_raw = new TH1D("h_mjj_raw","",100,0,500);
  TH1D *h_mjj_kin = new TH1D("h_mjj_kin", "", 100, 0, 500); 

  TH1D *h_nbjets_kin = new TH1D("h_nbjets_kin","",20,0,20);
  TH1D *h_mjj_kin_1b = new TH1D("h_mjj_kin_1b","",100,0,500);
  TH1D *h_mjj_kin_2b = new TH1D("h_mjj_kin_2b","",100,0,500);

  TH1D *h_mll_kin =  new TH1D("h_mll_kin", "", 80, 0, 200);

  TH1D *h_cutflow =  new TH1D("h_cutflow", "", 7, 0, 7);

  // add jet selection helper
  JetSelectionHelper jet_selector;
  ElectronSelectionHelper electron_selector;
  MuonSelectionHelper muon_selector;

  asg::AnaToolHandle<IJetCalibrationTool> JetCalibrationTool_handle;
  JetCalibrationTool_handle.setTypeAndName("JetCalibrationTool/MyCalibrationTool");
  JetCalibrationTool_handle.setProperty("JetCollection","AntiKt4EMPFlow"                                                );
  JetCalibrationTool_handle.setProperty("ConfigFile"   ,"JES_MC16Recommendation_Consolidated_PFlow_Apr2019_Rel21.config");
  JetCalibrationTool_handle.setProperty("CalibSequence","JetArea_Residual_EtaJES_GSC_Smear"                             );
  JetCalibrationTool_handle.setProperty("CalibArea"    ,"00-04-82"                                                      );
  JetCalibrationTool_handle.setProperty("IsData"       ,false                                                           );
  JetCalibrationTool_handle.retrieve();

  // for counting events
  unsigned count = 0;

  // get the number of events in the file to loop over
  Long64_t numEntries(-1);
  if(argc >= 3) numEntries = std::atoi(argv[2]);
  if(numEntries == -1) numEntries = event.getEntries();

  std::cout << "Processing " << numEntries << " events" << std::endl;

  // primary event loop
  for ( Long64_t i=0; i<numEntries; ++i ) {

    // Load the event
    event.getEntry( i );

    int n_bjet = 0;

    // Load xAOD::EventInfo and print the event info
    const xAOD::EventInfo * ei = nullptr;
    event.retrieve( ei, "EventInfo" );
    std::cout << "Processing run # " << ei->runNumber() << ", event # " << ei->eventNumber() << std::endl;

    // retrieve the jet container from the event store
    const xAOD::JetContainer* jets = nullptr;
    event.retrieve(jets, "AntiKt4EMPFlowJets_BTagging201810");

    const xAOD::ElectronContainer *electrons = nullptr;
    event.retrieve(electrons, "Electrons" ) ;
    const xAOD::MuonContainer *muons = nullptr;
    event.retrieve(muons, "Muons") ;

    // make temporary vector of jets for those which pass selection
    std::vector<xAOD::Jet> jets_raw;
    std::vector<xAOD::Jet> jets_kin;

    std::vector<xAOD::Electron> electrons_kin;
    std::vector<xAOD::Muon > muons_kin;

    // loop through all of the jets and make selections with the helper
    for(const xAOD::Jet* jet : *jets) {
      // calibrate the jet
      xAOD::Jet *calibratedjet;
      JetCalibrationTool_handle->calibratedCopy(*jet,calibratedjet);

      // print the kinematics of each jet in the event
      std::cout << "Jet : " << calibratedjet->pt() << calibratedjet->eta() << calibratedjet->phi() << calibratedjet->m() << std::endl;

      jets_raw.push_back(*calibratedjet);

      // perform kinematic selections and store in vector of "selected jets"
      if(jet_selector.isJetGood(calibratedjet)){
        jets_kin.push_back(*calibratedjet);
	
	if( jet_selector.isJetBFlavor(calibratedjet)){
	  n_bjet ++;
	}
      }

      delete calibratedjet;
    }

    for(const xAOD::Electron* electron : *electrons){
      if (electron_selector.isElectronGood(electron)){
	  electrons_kin.push_back(*electron);
      }
      std::cout << "Electron : " << electron->pt() << "\t" << electron->eta() << "\t" << electron->phi() << "\t" << electron->m() << "\t" << electron->charge() << std::endl;
    }

    for (const xAOD::Muon* muon : *muons){
      if (muon_selector.isMuonGood(muon)){
	muons_kin.push_back(*muon);
      }
    }
    

    // fill the analysis histograms accordingly
    h_njets_raw->Fill( jets_raw.size() );

    if( jets_raw.size()>=2 ){
      h_mjj_raw->Fill( (jets_raw.at(0).p4()+jets_raw.at(1).p4()).M()/1000. );
    }

    h_njets_kin->Fill( jets_kin.size() );

    if( jets_kin.size() >= 2){
      h_mjj_kin->Fill( (jets_kin.at(0).p4()+jets_kin.at(1).p4()).M()/1000. );
      h_nbjets_kin->Fill(n_bjet);

      if (n_bjet >= 1){
	h_mjj_kin_1b->Fill( (jets_kin.at(0).p4()+jets_kin.at(1).p4()).M()/1000. );
      }
      if (n_bjet >= 2){
	h_mjj_kin_2b->Fill( (jets_kin.at(0).p4()+jets_kin.at(1).p4()).M()/1000. );
      } 
      
    }

    if ( (electrons_kin.size() == 2 && electrons_kin.at(0).charge() != electrons_kin.at(1).charge()) or ( muons_kin.size() == 2 && muons_kin.at(0).charge() != muons_kin.at(1).charge())) { 
      if (electrons_kin.size() == 2){
	h_mll_kin->Fill( (electrons_kin.at(0).p4()+electrons_kin.at(1).p4()).M()/1000.);
      }
      else{
	h_mll_kin->Fill( (muons_kin.at(0).p4()+muons_kin.at(1).p4()).M()/1000.);
      }
    }

    // counter for the number of events analyzed thus far
    count += 1;

    h_cutflow->Fill(0);

    //Higgs selection
    // at least 2 jets
    if ( !(jets_kin.size() >= 2) ) continue;
    h_cutflow->Fill(1);

    if ( !(n_bjet >=2) ) continue;
    h_cutflow->Fill(2);
    
    if ( !(electrons_kin.size() == 2 && electrons_kin.at(0).charge() != electrons_kin.at(1).charge())) continue;
    h_cutflow->Fill(3);

    if ( !(std::abs( (jets_kin.at(0).p4()+jets_kin.at(1).p4()).M()/1000. - 125.) < 25)) continue;
    h_cutflow->Fill(4);
    
    if ( !(std::abs( (electrons_kin.at(0).p4()+electrons_kin.at(1).p4()).M()/1000. - 90) < 10.)) continue;
    h_cutflow->Fill(5);

  }

  // open TFile to store the analysis histogram output
  TFile *fout = new TFile("myOutputFile.root","RECREATE");

  h_njets_raw->Write();

  h_mjj_raw->Write();

  h_njets_kin->Write();
  
  h_mjj_kin->Write();

  h_nbjets_kin->Write();
  h_mjj_kin_1b->Write();
  h_mjj_kin_2b->Write();

  h_mll_kin->Write();

  h_cutflow->Write();


  fout->Close();

  // exit from the main function cleanly
  return 0;
}
